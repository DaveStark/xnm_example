﻿using System.Threading.Tasks;
using Moq;
using Ninject;
using Ninject.MockingKernel;
using Ninject.MockingKernel.Moq;
using Xunit;

namespace Services.Tests
{
    public class ExecutorsTest
    {

        public ExecutorsTest()
        {
            
        }

        [Fact]
        public void ShouldExecuteMoq()
        {
            var kernelMoq = new MoqMockingKernel();
            //kernelMoq.Bind<IHttpWebUtils>().To<HttpWebUtils>();
            //kernelMoq.Bind<IHttpWebUtils>().ToMock();
            var moq = kernelMoq.GetMock<IHttpWebUtils>();
           var task = new TaskCompletionSource<string>();
            task.SetResult(" fake data mocked !!!");
            moq.Setup(mock => mock.SendRequest(It.IsAny<string>())).Returns(task.Task);
            var executor = kernelMoq.Get<Executor>();
            executor.Execute("https://jsonplaceholder.typicode.com/posts");
            Assert.True(true);
        }

        [Fact]
        public void ShouldExecute()
        {
            var kernel = new StandardKernel();
            //kernel.Load<Bindings>();
            kernel.Bind<IHttpWebUtils>().To<HttpWebUtils>();
            var executor = kernel.Get<Executor>();
            executor.Execute("https://jsonplaceholder.typicode.com/posts");
            Assert.True(true);

            
        }
    }
}


// > Install-Package Ninject.MockingKernel.Moq