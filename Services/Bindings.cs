﻿using Ninject.Modules;

namespace Services
{
    public class Bindings : NinjectModule
    {
        public override void Load()
        {
            Bind<IHttpWebUtils>().To<HttpWebUtils>();
        }
    }
}
