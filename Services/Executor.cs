﻿using System.IO;
using Ninject.Modules;

namespace Services
{
    public class Executor 
    {

        private readonly IHttpWebUtils _httpWebUtils;

        public Executor(IHttpWebUtils httpWebUtils)
        {
            _httpWebUtils = httpWebUtils;
        }

        public void Execute(string url)
        {
            var taskResult = _httpWebUtils.SendRequest(url);
            taskResult.Wait();
            var result = taskResult.Result;
        }

        public async void CreateFile(string content)
        {
            string fullFilePath = Path.Combine(Path.GetTempFileName(), Path.GetTempPath());
            FileStream stream = File.Create(fullFilePath);
            using (var writer = new StreamWriter(stream))
            {
                writer.Write(content);
                writer.Flush();
                writer.Close();
            }
        }

        
    }
}
