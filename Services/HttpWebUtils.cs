﻿using System;
using System.Net.Http;
using System.Threading.Tasks;


namespace Services
{
    public class HttpWebUtils : IHttpWebUtils
    {
        public async Task<string> SendRequest(string url)
        {

            // ... Use HttpClient.
            using (HttpClient client = new HttpClient())
            using (HttpResponseMessage response = await client.GetAsync(url))
            using (HttpContent content = response.Content)
            {
                // ... Read the string.
                return await content.ReadAsStringAsync();
            }
        }
    }
}
